class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.string :article_name
      t.text :article_body
      t.string :article_type
      t.integer :upvotes
      t.datetime :postdate
      t.string :main_img_url

      t.timestamps
    end
  end
end
