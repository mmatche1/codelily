require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:one)
  end

  test "should get index" do
    get articles_url
    assert_response :success
  end

  test "should get new" do
    get new_article_url
    assert_response :success
  end

  test "should create article" do
    assert_difference('Article.count') do
      post articles_url, params: { article: { article_body: @article.article_body, article_name: @article.article_name, article_type: @article.article_type, main_img_url: @article.main_img_url, postdate: @article.postdate, upvotes: @article.upvotes } }
    end

    assert_redirected_to article_url(Article.last)
  end

  test "should show article" do
    get article_url(@article)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_url(@article)
    assert_response :success
  end

  test "should update article" do
    patch article_url(@article), params: { article: { article_body: @article.article_body, article_name: @article.article_name, article_type: @article.article_type, main_img_url: @article.main_img_url, postdate: @article.postdate, upvotes: @article.upvotes } }
    assert_redirected_to article_url(@article)
  end

  test "should destroy article" do
    assert_difference('Article.count', -1) do
      delete article_url(@article)
    end

    assert_redirected_to articles_url
  end

  test "should get blog" do
    get blog_url
    assert_response :success
  end

  test "should like article" do
    assert_difference '@article.upvotes', 1 do
      post like_article_url(@article)
      @article = @article.reload
    end
  end

end
