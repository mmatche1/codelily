require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest

  setup do
 	@tutorial = articles(:one)
  	@code_article = articles(:two)
  	@life_article = articles(:three)
  end

  test "should get index" do
    get home_index_url
    assert_response :success
  end

end
