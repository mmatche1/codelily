json.extract! article, :id, :article_name, :article_body, :article_type, :upvotes, :postdate, :main_img_url, :created_at, :updated_at
json.url article_url(article, format: :json)
