class HomeController < ApplicationController
  def index
  	@tutorial = Article.where(article_type: 'Tutorial').order(:upvotes).last
  	@code_article = Article.where(article_type: 'Code').order(:postdate).last
  	@life_article = Article.where(article_type: 'Lifestyle').order(:postdate).last
  end
end
