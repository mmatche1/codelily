// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require turbolinks
//= require_tree .

// article_id = document.getElementsByClassName('article').id
// // $(document).ready(function() {
// 	// $(".likes .btn").click(function()	{
// 		// if (article_id == $('.btn'.params('article_id'))) {
// 			// $('.fa-heart').css('color', '#C5829C');
// 			// $('.like-num .likes').css('color', 'black');
// 		//}
// 	// });
// // });
// //var button = document.getElementsByClassName("btn-like")
// $(document).ready(function() {
// 	$(".likes .btn").click(function()	{
// 		$('.fa-heart').css('color', '#C5829C');
// 		alert(article_id);
// 	});
// });



// document.ready(function() {
// 	var true_filter = getElementById("true-filter");
// 	var false_filter = getElementById("false-filter");
	
// 	true_filter.click(function() {
// 		true_filter.toggleClass("clicked");
// 	});

// 	true_filter.click(function() {
// 		true_filter.toggleClass("clicked");
// 	});
// });

$(document).ready(function() {
	$(".code").click(function() {
		$('.code').addClass("selected");
		$('.all').removeClass("selected");
		$('.life').removeClass("selected");
	});
	$(".life").click(function() {
		$('.life').addClass("selected");
		$('.all').removeClass("selected");
		$('.code').removeClass("selected");
	});
	$(".all").click(function() {
		$('.all').addClass("selected");
		$('.life').removeClass("selected");
		$('.code').removeClass("selected");
	});
});
