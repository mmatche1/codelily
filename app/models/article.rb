class Article < ApplicationRecord

	scope :article_type, -> (article_type) { where(article_type: article_type) }
	
	#scope :article_type_without_tutorials, -> { article_type.where.not(article_type: 'Tutorial') }
	#def self.filter(code, life)
	#	if code && life
	#		where("article_type = 'Code' AND article_type = 'Lifestyle'").order('postdate DESC')
	#	elsif !code && life
	#		where("article_type = 'Lifestyle'").order('postdate DESC')
	#	elsif code && !life
	#		where("article_type = 'Code'").order('postdate DESC')
	#	elsif !code && !life
	#		where("article_type = ''")
	#	end
	#end
end		

