Rails.application.routes.draw do
  resources :users
  resources :articles
  resources :articles do
  	post 'like', on: :member
  end
  get 'home/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'

  get "/pages/:page" => "pages#show"

  get "/blog" => "articles#blog"
  get "/tutorials" => "articles#tutorials"

  get "/blog" => "articles#filter_articles"

  put "/blog" => "articles#like"

end
